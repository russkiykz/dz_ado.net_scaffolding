CREATE database HomeworkScaffolding

use HomeworkScaffolding

CREATE table Article
(
	Id uniqueidentifier primary key,
	[Name] nvarchar(max) null,
	CategoryId uniqueidentifier not null
	constraint PK_Article_Categories foreign key (CategoryId)
	references Categories(Id),
	AuthorId uniqueidentifier not null
	constraint PK_Article_Authors foreign key (AuthorId)
	references Authors(Id),
)

CREATE table Categories
(
	Id uniqueidentifier primary key,
	[Name] nvarchar(max) null
)

CREATE table Authors
(
	Id uniqueidentifier primary key,
	[FullName] nvarchar(max) null
)


INSERT INTO HomeworkScaffolding.dbo.Article
(Id, Name, CategoryId, AuthorId)
VALUES('717628AF-945A-4648-BB49-002FD3EF4EA2', N'� ����� �������', '6BB3DD2A-9148-4F99-AECA-BF7F9C2C0D4B', '37CE3184-F4F4-4370-97BE-243DFB14359F');
INSERT INTO HomeworkScaffolding.dbo.Article
(Id, Name, CategoryId, AuthorId)
VALUES('09033D89-9B1A-48DA-84F6-A58CF340520E', N'� ������ �����������', '6BB3DD2A-9148-4F99-AECA-BF7F9C2C0D4B', '2673D172-91DF-427E-9055-2264E1A004ED');
INSERT INTO HomeworkScaffolding.dbo.Article
(Id, Name, CategoryId, AuthorId)
VALUES('B8CB0E69-25F1-4BE6-9DE1-EC550167BFE1', N'������ - ������� ����!', '17D3B081-74E6-4FA7-A454-828656EFC5F7', 'C04D2F06-E207-4950-9D69-08F0701072E4');
INSERT INTO HomeworkScaffolding.dbo.Article
(Id, Name, CategoryId, AuthorId)
VALUES('8BAB6972-4FC0-436A-9391-F444D73F2BD9', N'WiFi-6 ��� ���������� ��������', 'C3063E9B-1A83-448E-851C-3A291AF892D1', '0A8FFD9B-2DA8-4D33-A0E8-8ABA8C6F2997');



INSERT INTO HomeworkScaffolding.dbo.Authors
(Id, FullName)
VALUES('2673D172-91DF-427E-9055-2264E1A004ED', N'�.������');
INSERT INTO HomeworkScaffolding.dbo.Authors
(Id, FullName)
VALUES('37CE3184-F4F4-4370-97BE-243DFB14359F', N'�.��������');
INSERT INTO HomeworkScaffolding.dbo.Authors
(Id, FullName)
VALUES('C04D2F06-E207-4950-9D69-08F0701072E4', N'�.�������');
INSERT INTO HomeworkScaffolding.dbo.Authors
(Id, FullName)
VALUES('0A8FFD9B-2DA8-4D33-A0E8-8ABA8C6F2997', N'�.������');

INSERT INTO HomeworkScaffolding.dbo.Categories
(Id, Name)
VALUES('6BB3DD2A-9148-4F99-AECA-BF7F9C2C0D4B', N'��������');
INSERT INTO HomeworkScaffolding.dbo.Categories
(Id, Name)
VALUES('17D3B081-74E6-4FA7-A454-828656EFC5F7', N'�����');
INSERT INTO HomeworkScaffolding.dbo.Categories
(Id, Name)
VALUES('C3063E9B-1A83-448E-851C-3A291AF892D1', N'����������');



SELECT * from Article a 
