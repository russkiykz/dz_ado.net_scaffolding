﻿using System;
using System.Collections.Generic;

#nullable disable

namespace HomeworkScaffolding
{
    public partial class Article
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid CategoryId { get; set; }
        public Guid AuthorId { get; set; }
        public DateTime DateOfPublication { get; set; } = DateTime.Now;

        public virtual Author Author { get; set; }
        public virtual Category Category { get; set; }
    }
}
